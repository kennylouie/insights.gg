# Live Session Backend

## Background

A backend architecture for processing live session data.

![](assets/ow_live_session_1.png)

This design is to add user features for the insights.gg live session product (https://insights.gg/). The live session allows gamers to collaborate in real time with multiple team members to watch a video and share comments and/or annotations overlaid on top of the video. As of the time of writing (April 2020), the live session does not allow replay of the live session nor export of any data. The only addition feature on top of live session is the ability for users to make snapshots of the current annotations at a timepoint in the video. However, once the live session is over, the events that occurred during the session are lost to the host and the attending members.

Example of the event data generated from a live session:
```
{"type":"video","user":"3Q271pCmAOQMQRpbt5804g","action":"seek","position":0}
```

```
{"type":"annotation","user":"3Q271pCmAOQMQRpbt5804g","action":"object:added","object":{"type":"rect","version":"3.5.1","originX":"left","originY":"top","left":74.88,"top":127.27,"width":200.39,"height":163.83,"fill":"","stroke":"#FF0000","strokeWidth":3,"strokeDashArray":null,"strokeLineCap":"butt","strokeDashOffset":0,"strokeLineJoin":"miter","strokeMiterLimit":4,"scaleX":1,"scaleY":1,"angle":0,"flipX":false,"flipY":false,"opacity":1,"shadow":null,"visible":true,"clipTo":null,"backgroundColor":"","fillRule":"nonzero","paintFirst":"fill","globalCompositeOperation":"source-over","transformMatrix":null,"skewX":0,"skewY":0,"rx":0,"ry":0,"uuid":"8651d792-dd18-4df8-b9e9-e65bd3af7f7f"}}
```

*Where a live session can range from 2 events to >80k.

## Aim

The core of this design is to leverage a NoSQL datastore to store and search the event data.

The aim of this design doc is to present a backend architecture to expose the event data. Specifically, the following will be discussed:

+ service to read events data from google cloud storage (if no frontend API is available to push to message queue)
+ scalably receive events to store as a queue
+ queue consuming service to index events into a NoSQL datastore
+ RESTful API as a query service for retrieving event types
+ machine learning service to read annotation data and predict complete images
+ service to repiece annotations into full images
+ SQL datastore as a cold storage option for reliability

The results of the following technical design will be the following:

+ users will be able to export the chat log from a specific live session
+ users will be able to export images of full drawings from the live session
+ users will be able to replay the entire live session provided that the video and redrawing program are available on the frontend
+ users will be able to replay a live session and add addition interactivity, e.g. annotations, to it thus altering the live session

## Design

The design as presented will focus on and leverage on the following technologies:

+ elasticsearch
+ kafka
+ go

However, alternatives of similar technologies can be used in place, e.g. MongoDB instead of ES.

### Architecture

The following is the proposed backend architecture. Note, this diagram does not account for cloud infrastructure (e.g. load balancers, pub/private subnets, proxies, cdn, etc...)

```
  Assumptions
  +--------------------+--------------------+--------------------------------------------------------------------------------------------------------------+
  |+------------------+| +----------------+ |           +--------------------+            +--------------------+           +-----------------------------+ |
  ||     Frontend     ++->      API       |-+---------->|                    |----------->|                    |-----+---->|                             | |
  ||  Live session    || |                | |      or   |       Kafka        |            |  Indexing service  |     |     |                             | |
  |+------------------+| +^-+-------------+ |      ---->|                    |            |                    |     |     |    elasticsearch cluster    | |
  |+------------------+|  | |               |      |    +--------------------+            +--------------------+     |     |                             | |
  ||     Frontend     ||  | |               |      |                    |                                            |     |                             | |
  ||  Live session    ++--+ |               |      |               +----+--------------------------------------------+---->|                             | |
  |+------------------+|    |               |      |               |    |                                            |     +-----------------------------+ |
  |       ...          |    |               |      |               |    |                                            |                                     |
  +-------------------------+---------------+      |               |    |                                            |     +-----------------------------+ |
          *json events |    |                      |               |    +------------------------------------------+ |     |                             | |
           +-----------+----+                      |               |                                               | *---->|            PSQL             | |
           |           |                           |               v                                               |       |                             | |
  +--------v---------+ |  +--------------------+   |   +------------------------+        +-----------------------+ |       +-----------------------------+ |
  |   Google Cloud   | |  |    *Event          |   |   |                        |        |                       | |                                      |
  |     Storage      |<+->| Producing Service  |---+   |                        |        |                       | |             +-----------------------+ |
  |                  | |  |                    |       |       Query API        <--------+    Imager Service     | |             |                       | |
  +------------------+ |  +--------------------+       |                        |        |                       | +------------>|      ML Service       | |
           ^           |                               |                        |        |                       <---------------|                       | |
           |           |                               +------------------------+        +--+--------------------+               +-----------------------+ |
           +-----------+-------------------------------------------+------------------------|                                                              |
                       +-------------------------------------------+---------------------------------------------------------------------------------------+
  +-------------------+                                            |
  | Frontend          |                                            |
  | Features:         |                                            |
  | + Export Chat     |<-------------------------------------------+
  | + Replay annots   |
  |   in order at     |
  |   variable speed  |
  +-------------------+

```

### Event Producing Service

This service is only needed if the backend API to the current front end live feature is not available to produce events to kafka. If working on top of the current situation (json files in google cloud storage), then this service would have the following:

1. a gcp client connection to store live session files
2. a cron job to check for new live sessions
3. a routine to read from the live session and produce these events as json to kafka
4. add timestamp and filenames to messages if its not possible from the frontend
5. a sql client connection
6. update same table that keeps track of live sessions indicating session has been produced to backend

### Kafka

Notes on Kafka:

1. Kafka is used as a first layer switch to the data pipeline
2. It can scale relatively easily
3. Can act as a temporary storage as it stores events up to a certain limit
4. Because of this, it can be used to rehydrate data into elasticsearch

### Indexing service

A stateless microservice that consumes messages from kafka topics and indexes into elasticsearch. The purpose of this service is to act as a throttle/switch for data flowing into elasticsearch.

1. a kafka client connection
2. subscription to the main live session topic
3. an elasticsearch client connection
4. polls a bulk amount of messages (depends on total bulk size, elasticsearch has an optimal amount per bulk, needs fine tuning)
5. bulk indexes messages into elasticsearch when bulk amount is reached
6. if failure, sends bulk to a retry topic
7. if multiple failures, messages are sent to a dead letter queue
8. a psql client connection
9. sends all copies of events to psql for cold storage

### Elasticsearch

Notes on elasticsearch:

1. indices should be created temorally, i.e. by week or month, this would need to be fine tuned via metrics
2. elasticsearch works optimally by storing documents as events or logs overtime without editing the documents
3. elasticsearch can scale by cluster if needed, but is computationally expensive
4. elasticsearch can also be managed by 3rd party vendors such as ElasticCloud
5. shards should be replicated on indices/months that are more frequently queried, e.g. recent live session events after major tournament

### Query API

A stateless microservice RESTful API to request event data from live sessions.

1. an elasticsearch client connection
2. expose endpoints:
    a. `GET /annotations/:id`

      i. `GET /annotations/:id/:uid`

    b. `GET /chat/:id`

      i. `GET /chat/:id/:uid`

    c. `GET /videoevents/:id`

      i. `GET /videoevents/:id/:uid`

where `:id` is the live session id and `uid` is the specific event's id

The endpoint leverages Elasticsearch's queries, for example for `GET /chat/:id`:
```
GET /__INDEX__/search
{
    "sort": {
        "timestamp": "asc"
    },
    "query": {
        "bool": {
            "must": [
                {
                    "match": {
                        "SessionId": ":id"
                    }
                },
                {
                    "term": {
                        "type": "chat"
                    }
                }
            ]
        }
    }
}
```

The response will be an array of events for type `chat` for session id `:id`

### Machine learning service

A stateless service that uses a pretrained ML model trained to predict the range of events (begin to end) that creates a full annotation picture.

1. a kafka client connection
2. consumes from a kafka topic that is produced from the producing service/frontend API
3. message consumes contains the live session id
4. on a single thread, requests the query API for the annotations from the live session
5. if failure, sends live session message to a retry/dead letter topic
6. predicts full annotations given live session id
7. sends a completed message to the topic for imager
8. duplicates message to psql for cold storage

### Imager Service

A stateless service that takes pairs of annotation events and produces full images with all annotations between those time points. Images are uploaded to a static store.

1. a kafka client connection
2. consumes from a kafka topic sent by ML service
3. using pairs of annotations, requests the query API for annotations between those time points
4. using those annotations, redraws the entire image
5. images are sent a static store like S3 or google cloud storage

** optionally can overlay on top of video if video data is available

## Scalability

Opportunities for horizontal scaling:

+ Kafka
+ Query API
+ Elasticsearch
+ warming of servers (scaling) on busier nights such as after an OW league weekend (e.g. Monday evening)

## Caveats

Some assumptions were made regarding the current state of the system. Specifically, the data is assumed to be able to be revised with the following:

1. a live session id needs to be added as a field
2. a timestamp should be generated for each event payload
3. careful schema design for certain fields (the user field between the annotation type and connection type are differently structured)

Given the changes, the backend will be more than readily available to work with such data.
